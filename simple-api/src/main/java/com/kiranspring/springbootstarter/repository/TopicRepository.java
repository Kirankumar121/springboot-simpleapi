package com.kiranspring.springbootstarter.repository;

import org.springframework.data.repository.CrudRepository;

import com.kiranspring.springbootstarter.beans.Topic;

public interface TopicRepository extends CrudRepository<Topic, String> {
	
	//getAllTopics()
	//getTopic(String Id)
	//updateTopic(Topic,Id)
	//deleteTopic(String Id)

}

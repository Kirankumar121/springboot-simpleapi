package com.kiranspring.springbootstarter.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String sayRoot(){
		
		return "Hello Root";
	}
	
	@RequestMapping("/hello")
	public String sayHello(){
		
		return "Hello Kiran";
	}

}

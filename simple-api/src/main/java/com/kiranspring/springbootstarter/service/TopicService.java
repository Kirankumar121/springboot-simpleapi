package com.kiranspring.springbootstarter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kiranspring.springbootstarter.beans.Topic;
import com.kiranspring.springbootstarter.repository.TopicRepository;

@Service
public class TopicService {
	
	@Autowired
	TopicRepository topicRepository;

	public List<Topic> getAllTopics(){
		List<Topic> topicList = new ArrayList<>();
		topicRepository.findAll().forEach(topicList::add);
		return topicList;
	}
	
	public Optional<Topic> getTopicById(String id){
		
		return topicRepository.findById(id);
	}
	
	public void addTopic(Topic topic){
		
		topicRepository.save(topic);
	}

	public void updateTopic(String id, Topic topic) {
		
		topicRepository.save(topic);
	
	}

	public void deleteTopic(String id) {
		
		topicRepository.deleteById(id);
	}

}
